import os

def main():
	d = {}
	f = open('input.txt')
	fline = int(f.readline().strip('\r\n'))
	for el in [line.strip('\r\n') for line in f.readlines() ]:
		d[el] = len(el)
	f.close
	print '\n'.join( sorted(d, key = d.__getitem__, reverse = True)[:fline])

if __name__ == '__main__':
	main()
