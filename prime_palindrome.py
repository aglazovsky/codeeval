#Write a program which determines the largest prime palindrome less than 1000.
import math
def is_prime(n):
	i = 2
	while i <= int(math.sqrt(n)):
		if n%i == 0:
			return False
		else:
			i += 1
	return True
	
def biggest_palindrome():
	pal_list = [i for i in xrange(1, 1000) if i == int(str(i)[::-1]) and is_prime(i) == True]
	print max(pal_list)
	
# biggest_palindrome()

