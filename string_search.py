#!/usr/bin/env python

import re
import sys

def main(data):
    f = open(data)
    lines = f.read().splitlines()
    for i in [line.split(',') for line in lines]:
	try:
		if re.findall(i[1],i[0]) != []:
             		print('true')
		else: print('false')
	except re.error:
		print i

if __name__ == '__main__':
    main(sys.argv[1])
