#Print the sum of integers read from a file
def intsum(file):
        text = open(file).read().splitlines()
	print sum([i for i in text])
